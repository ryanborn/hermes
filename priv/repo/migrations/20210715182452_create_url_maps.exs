defmodule Hermes.Repo.Migrations.CreateUrlMaps do
  use Ecto.Migration

  def change do
    create table(:url_maps) do
      add :shortened, :string
      add :real, :string

      timestamps()
    end

  end
end
