defmodule Hermes.Factory do
  @moduledoc false
  use ExMachina.Ecto, repo: Hermes.Repo

  def url_map_factory do
    %Hermes.Models.URLMap{
      shortened: sequence(:url_map_shortened, &"abcd#{&1}"),
      real: "https://google.com"
    }
  end
end
