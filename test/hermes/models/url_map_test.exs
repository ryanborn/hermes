defmodule Hermes.Models.URLMapTest do
  use Hermes.DataCase

  alias Hermes.Models.URLMap

  test "Can create new url_map" do
    changeset =
      %URLMap{}
      |> URLMap.changeset(%{
        shortened: "abcd123",
        real: "https://phoenixframework.org/"
      })

    assert changeset.valid?
  end

  describe "All fields are required" do
    test "shortened" do
      changeset =
        %URLMap{}
        |> URLMap.changeset(%{
          real: "https://phoenixframework.org/"
        })

      refute changeset.valid?

      assert changeset.errors == [shortened: {"can't be blank", [validation: :required]}]
    end

    test "real" do
      changeset =
        %URLMap{}
        |> URLMap.changeset(%{
          shortened: "abcd123"
        })

      refute changeset.valid?

      assert changeset.errors == [real: {"can't be blank", [validation: :required]}]
    end
  end

  test "real format validation" do
    changeset =
      %URLMap{}
      |> URLMap.changeset(%{
        shortened: "abcd123",
        real: "wrong_format"
      })

    refute changeset.valid?

    assert changeset.errors == [real: {"has invalid format", [validation: :format]}]

    changeset =
      %URLMap{}
      |> URLMap.changeset(%{
        shortened: "abcd123",
        real: "http://"
      })

    refute changeset.valid?

    assert changeset.errors == [real: {"has invalid format", [validation: :format]}]

    changeset =
      %URLMap{}
      |> URLMap.changeset(%{
        shortened: "abcd123",
        real: "http://google.com"
      })

    assert changeset.valid?

    changeset =
      %URLMap{}
      |> URLMap.changeset(%{
        shortened: "abcd123",
        real: "https://google.com"
      })

    assert changeset.valid?
  end
end
