defmodule Hermes.URLMapsTest do
  use Hermes.DataCase
  import Hermes.Factory

  alias Hermes.URLMaps

  test "get correct url_map" do
    inserted = insert(:url_map)

    fetched = URLMaps.get_by_shortened_url(inserted.shortened)

    assert fetched == inserted

    fetched = URLMaps.get_by_real_url(inserted.real)

    assert fetched == inserted
  end

  test "create url_map correctly" do
    real_url = "https://www.phoenixframework.org"
    inserted = URLMaps.new(real_url)

    assert inserted.real == real_url

    assert URLMaps.get_by_real_url(real_url) == inserted
  end

  test "create url map with duplicate url returns same url_map" do
    real_url = "https://www.phoenixframework.org"
    inserted = URLMaps.new(real_url)

    assert inserted.real == real_url

    duplicate = URLMaps.new(real_url)

    assert duplicate == inserted
  end
end
