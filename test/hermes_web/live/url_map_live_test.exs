defmodule HermesWeb.PageLiveTest do
  use HermesWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, url_map_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "Enter URL"
    assert render(url_map_live) =~ "Enter URL"
  end

  test "submit valid url succeeds", %{conn: conn} do
    {:ok, url_map_live, _} = live(conn, "/")

    assert url_map_live
           |> element("form")
           |> render_submit(%{"url_map" => %{"real" => "https://phoenixframework.org"}}) =~
             "Success!"
  end

  test "submit invalid url fails", %{conn: conn} do
    {:ok, url_map_live, _} = live(conn, "/")

    assert url_map_live
           |> element("form")
           |> render_submit(%{"url_map" => %{"real" => "fake_url"}}) =~
             "The URL provided is invalid"
  end
end
