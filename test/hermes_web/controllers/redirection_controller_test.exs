defmodule HermesWeb.RedirectionControllerTest do
  use HermesWeb.ConnCase
  import Hermes.Factory

  test "renders the not found page when nothing matches", %{conn: conn} do
    conn = get(conn, Routes.redirection_path(conn, :index, "fake"))

    assert html_response(conn, 404) =~ "Not Found"
  end

  test "redirects to appropriate url when shortened matches", %{conn: conn} do
    url_map =
      insert(:url_map, %{
        shortened: "abcd123",
        real: "https://phoenixframework.org"
      })

    conn = get(conn, Routes.redirection_path(conn, :index, url_map.shortened))

    assert redirected_to(conn) == url_map.real
  end
end
