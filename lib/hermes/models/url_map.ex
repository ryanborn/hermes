defmodule Hermes.Models.URLMap do
  @moduledoc """
  Schema info for the URLMap model
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "url_maps" do
    field :real, :string
    field :shortened, :string

    timestamps()
  end

  @doc false
  def changeset(url_map, attrs) do
    url_map
    |> cast(attrs, __schema__(:fields))
    |> validate_required([:shortened, :real])
    |> validate_format(:real, ~r/https?:\/\/.+/)
  end
end
