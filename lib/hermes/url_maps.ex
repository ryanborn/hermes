defmodule Hermes.URLMaps do
  @moduledoc """
  Context module for dealing with URL Maps
  """

  alias Hermes.Models.URLMap
  alias Hermes.Repo

  def get_by_shortened_url(shortened_url) do
    URLMap
    |> Repo.get_by(shortened: shortened_url)
  end

  def get_by_real_url(real_url) do
    URLMap
    |> Repo.get_by(real: real_url)
  end

  def new(real_url) do
    case get_by_real_url(real_url) do
      nil ->
        %URLMap{}
        |> URLMap.changeset(%{
          real: real_url,
          shortened: Nanoid.generate()
        })
        |> Repo.insert!()

      url_map ->
        url_map
    end
  end
end
