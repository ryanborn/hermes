defmodule HermesWeb.URLMapLive do
  @moduledoc """
  Live view for URLMap creation
  """
  use HermesWeb, :live_view

  alias Hermes.URLMaps
  alias HermesWeb.ViewModels.URLMap, as: URLMapViewModel

  @impl true
  def mount(_params, _session, socket) do
    {:ok, reset_assigns(socket)}
  end

  @impl true
  def handle_event("reset", _, socket) do
    {:noreply, reset_assigns(socket)}
  end

  @impl true
  def handle_event("submit", %{"url_map" => params}, socket) do
    changeset = %URLMapViewModel{} |> URLMapViewModel.changeset(params)

    submit(socket, changeset, params)
  end

  defp reset_assigns(socket) do
    changeset = %URLMapViewModel{} |> URLMapViewModel.changeset()
    assign(socket, %{changeset: changeset, url_map: nil})
  end

  defp submit(socket, %{valid?: true}, %{"real" => real_url}) do
    url_map = URLMaps.new(real_url)

    {:noreply, assign(socket, %{changeset: nil, url_map: url_map})}
  end

  defp submit(socket, %{valid?: false}, _) do
    {:noreply, put_flash(socket, :error, "The URL provided is invalid")}
  end
end
