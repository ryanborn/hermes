defmodule HermesWeb.RedirectionController do
  @moduledoc """
  Controller that takes the shortened url and redirects the client to the corresponding "real" url
  """

  use Phoenix.Controller

  alias Hermes.Models.URLMap
  alias Hermes.URLMaps

  def index(conn, %{"shortened_url" => shortened_url}) do
    redirect_shortened(conn, URLMaps.get_by_shortened_url(shortened_url))
  end

  defp redirect_shortened(conn, %URLMap{real: real_url}), do: conn |> redirect(external: real_url)
  defp redirect_shortened(conn, _), do: conn |> put_status(404) |> render("not_found.html")
end
