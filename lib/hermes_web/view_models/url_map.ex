defmodule HermesWeb.ViewModels.URLMap do
  @moduledoc """
  View Model for URLMap creation
  """
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :real, :string
  end

  def changeset(url_map, attrs \\ %{}) do
    url_map
    |> cast(attrs, [:real])
    |> validate_required([:real])
    |> validate_format(:real, ~r/https?:\/\/.+/)
  end
end
