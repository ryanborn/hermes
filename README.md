# Hermes

Pre-requirements:

  * docker, docker-compose need to be installed
  * erlang/elixir to compile and run project
  * nodejs/npm to build static assets

To setup the project:

  * Run `make setup`
  * This spins up the postgres db in a docker container
  * It also installs dependencies and runs migrations

To run tests:

  * Run `make test`
  * This makes sure the postgres db is up and running
  * It then runs the test suite

To run server:

  * Run `make server`
  * This starts an iex session of the project accessible at `http://localhost:4000`


## Technological decisions

I feel some of my decisions are pretty self explanatory. I set up a model with a context module, used a generic controller for redirection, and LiveView for the form. These are what I know and it works rather well.

I started to implement some CI with gitlab runners (I'm using the free shared runners provided by Gitlab), I have the test stage working, and tried to get the build stage going, but got blocked by npm not being present on the image I was using. I decided I wouldn't go through the time of finding/creating and image that would fit the needs, but that's certainly something I could do. Deploying would depend on where things are being deployed to, if I need a docker image I would build it separately and push the image to where it needed to go, while the actual deploy would just instruct whatever orchestrator to use the needed image.

I used materializecss just to try to make things look a little better. I don't feel it worked that well. I can follow designs pretty well, I'm just not very good at coming up with them.

Thanks for your time!