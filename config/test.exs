use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :hermes, Hermes.Repo,
  url:
    System.get_env(
      "PG_WRITE_DATABASE_URL",
      "postgres://postgres:secret@localhost:5434/hermes_test"
    ),
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :hermes, HermesWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :junit_formatter,
  report_dir: "cover",
  report_file: "test-junit-report.xml",
  print_report_file: true,
  include_filename?: true,
  include_file_line: true
